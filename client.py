#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Módulo del cliente UDP que abre un socket a un servidor."""

import sys
import socket

def check_peticion():
    """Método usado para darle formato a la peticion del cliente"""
    if REGISTER == 'REGISTER':
        registertxt = REGISTER + ' sip:' + SIP_ADDRESS + ' SIP/2.0\r\n'
        exipirationtxt = 'Expires: ' + EXPIRATION + '\r\n\r\n'
        return registertxt + exipirationtxt


# ------------------------------------------------------------------------------
try:
    # En primer lugar, guardamos los parametros en variables constantes, ya que estos no sufriran modificaciones.
    IP = sys.argv[1]
    PORT = int(sys.argv[2])
    REGISTER = sys.argv[3]
    SIP_ADDRESS = sys.argv[4]
    EXPIRATION = sys.argv[5]

    # Creamos el socket
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        # Conectamos el socket
        sock.connect((IP, PORT))
        # Mostramos el mensaje del envio del registro y la expiracion
        print("Enviando:" + check_peticion(), end='')
        # Enviamos el socket
        sock.send(bytes(str(check_peticion()), 'utf-8'))
        data = sock.recv(1024)
        print('Recibiendo... \n', data.decode('utf-8'), end='')
    print("Socket terminado con exito")

# Por último, comprobamos algunos errores
except (IndexError, ValueError):
    print("Usage: client ip puerto register sip_address expires_value")
except ConnectionRefusedError:
    print("Servidor apagado")
