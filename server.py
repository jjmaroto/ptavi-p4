#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Módulo en el cual se encuetra el código para crear un servidor de eco en UDP simple."""
import socketserver
import json
import sys
import time

# guardamos el path del json en una variable constante
FILE_PATH = 'registered.json'


# Clase principal, la cual hereda los atributos del socket
class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Clase usada para responder a la peticion SIP de un cliente"""

    dic_clients = {}

    def json2register(self):
        """Metodo utilizado para abrir el archivo JSON y obtener su informacion"""
        try:
            with open('registered.json', 'r', encoding="utf-8") as file:
                self.dic_clients = json.load(file)
        except (FileNotFoundError, ValueError, json.decoder.JSONDecodeError):
            # el servidor seejecutara como si el fichero JSON no existiera, para ello, usamos pass
            pass

    def register2json(self):
        """Método utilizado para escribir la información del cliente en el archivo JSON"""
        try:
            with open(FILE_PATH, "w", encoding="utf-8") as file:
                json.dump(self.dic_clients, file)
        except FileNotFoundError:
            raise Exception("Fichero no encontrado")

    # ---------------------------------------------------------------------------------------------------------------
    # a continuación podemos encontrar una serie de métodos auxiliares usados para embellecer el código y no
    # alargar demasiado el handler, que es la función principal. Esto se debe a que, según los estándares,
    # si un método no cabe en pantalla, se considera como demasiado largo. Por ello, las siguientes 4 funciones
    # fueron creadas.
    def eliminar_expirados(self):
        """Método empleado para buscar y eliminar del diccionario aquellos clientes que ya han expirado"""
        # obtenemos el tiempo actual
        actual_time = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
        # iteramos a través de la lista de clientes (transformamos el diccionario en lista para evitar error de mutacion)
        for cliente in list(self.dic_clients):
            # si la fecha de caducidad del cliente es menor que la actual, eliminamos el cliente del diccionario
            if self.dic_clients[cliente]["expires"] <= actual_time:
                del self.dic_clients[cliente]

    def check_register(self,address_sip):
        """Método usado para responder la petición SIP del cliente """
        #enviamos la respuesta al socket
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        # finalmente, guardamos su adress en el diccionario del JSON
        self.dic_clients[address_sip] = {"address": self.client_address[0]}

    def direccion_cliente(self, decoded_line):
        """Método usado para obtener la direccion sip del cliente """
        client_sip = decoded_line[1].split(":")
        address_sip = client_sip[1]
        return address_sip

    def check_expired(self, address_sip, decoded_line):
        """Método empleado para comporbar la caducidad de los clientes"""
        #obtenemos la fecha de vencimiento
        expiration = decoded_line[1][:-2]
        exp = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time() + float((expiration))))
        # asignamos al correspndiente valor del JSON la fecha
        self.dic_clients[address_sip]["expires"] = exp
        # si la expiration date es 0, damos de baja al cliente eliminandolo
        if expiration == '0':
            del self.dic_clients[address_sip]
        else:
            # comprobamos tambien si ya hay clientes caducados y los eliminamos
            self.eliminar_expirados()

    # ----------------------------------------------------------------------------------------------------------
    def handle(self):
        """Metodo utilizado por el servidor para responder clientes"""
        # en primer lugar, comporbamos si el diccionario está vacío, en cuyo caso usaremos el método json2register para
        # llenarlo
        if self.dic_clients == {}:
            self.json2register()
        # comprobamos que la petición se haya recibido, en cuyo caso lo escribimos en el socket
        self.wfile.write(b"Hemos recibido la peticion ")
        # pasamos a leer linea por línea, obteniendo su información
        for line in self.rfile:
            decoded_line = line.decode('utf-8').split(" ")
            # en primer lugar, contestamos a las peticiones SIP, en caso de existir
            if decoded_line[0] == 'REGISTER':
                address_sip = self.direccion_cliente(decoded_line)
                self.check_register(address_sip)
            if decoded_line[0] == 'Expires:':
            # comprobamos los clientes con fecha expirada
                self.check_expired(address_sip, decoded_line)
        # finalmente actualizamos el JSON y avisamos mediante un print de que el usuario ha sido registrado
        print("Nuevo usuario registrado")
        self.register2json()


# //////////////////////////////////////////////////////////////////////////////////////////////////////////
if __name__ == "__main__":
    """Programa principal, donde se instancia la clase SIPRegisterHandler,
    indicando la IP y el puerto donde se deja al servidor escuchando
    en un bucle infinito (del que solo se puede salir desde el terminal
    con Ctrl+C, que lanza una excepcion KeyboardInterrupt"""

    try:
        port = int(sys.argv[1])
        serv = socketserver.UDPServer(('', port), SIPRegisterHandler)
        print("Lanzando servidor UDP...")
        try:
            serv.serve_forever()
        except KeyboardInterrupt:
            print("Servidor finalizado")
    except (IndexError, ValueError, PermissionError):
        print("Usage: phython3 server.py puerto")
